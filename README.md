# Jersey Connection Injector

Jersey Connection Injector provides an interface for injecting Connection resources into request, as well as automatically disposing of them once used.

## Getting Started

Just build and include the JAR!

## Usage

To use the connection injector, first register the connector using the ``ConnectionInjectBinder``. 
Then write a wrapper for your connection factory and optionally create a disposer for custom disposal of the resource. Then
register the wrapper with the ``ConnectionFactoryFactory`` with a key, and now any request with an ``@ConnectionInject(key)`` will
have a Connection from that factory injected!