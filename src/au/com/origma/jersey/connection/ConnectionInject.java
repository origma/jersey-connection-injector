package au.com.origma.jersey.connection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConnectionInject {
	/**
	 * Defines which Connection to request
	 * @return The name of the connection to request
	 */
	String value();
}
