package au.com.origma.jersey.connection.jersey;

import java.sql.Connection;
import java.util.function.Function;

import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.model.Parameter;
import org.glassfish.jersey.server.spi.internal.ValueParamProvider;

import au.com.origma.jersey.connection.ConnectionInject;
import au.com.origma.jersey.connection.factories.ConnectionFactoryFactory;

/**
 * Provides an ConnectionInjectFactory for the provided parameter
 * @author Ben McLean
 */
public class ConnectionInjectFactoryProvider implements ValueParamProvider{
	
	ConnectionFactoryFactory cff;

	public ConnectionInjectFactoryProvider(ConnectionFactoryFactory connectionFactoryFactory) {
		super();
		this.cff = connectionFactoryFactory;
	}
	
	@Override
	public PriorityType getPriority() {
		return Priority.NORMAL;
	}

	@Override
	public Function<ContainerRequest, ?> getValueProvider(Parameter param) {
		Class<?> paramType = param.getRawType();
        ConnectionInject annotation = param.getAnnotation(ConnectionInject.class);
        if (annotation != null && paramType.isAssignableFrom(Connection.class)) {
            return new ConnectionInjectFactory(cff.get(annotation.value()));
        }
        return null;
	}

}
