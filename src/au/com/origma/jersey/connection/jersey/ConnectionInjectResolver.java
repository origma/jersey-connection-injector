package au.com.origma.jersey.connection.jersey;

import javax.inject.Provider;

import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.internal.inject.ParamInjectionResolver;

import au.com.origma.jersey.connection.ConnectionInject;
import au.com.origma.jersey.connection.factories.ConnectionFactoryFactory;

/**
 * Resolves and manages ConnectionInject annotations
 * @author Ben McLean
 */
public class ConnectionInjectResolver extends ParamInjectionResolver<ConnectionInject> {
	
	public ConnectionInjectResolver(Provider<ContainerRequest> request) {
		super(new ConnectionInjectFactoryProvider(ConnectionFactoryFactory.getInstance()), ConnectionInject.class, request);
	}

}
