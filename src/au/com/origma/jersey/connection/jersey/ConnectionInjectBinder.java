package au.com.origma.jersey.connection.jersey;

import javax.inject.Singleton;

import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.TypeLiteral;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import au.com.origma.jersey.connection.ConnectionInject;

/**
 * Binds ConnectionInject
 * @author Ben McLean
 */
public class ConnectionInjectBinder extends AbstractBinder{

	public ConnectionInjectBinder() {
		super();
	}

	@Override
	protected void configure() {
		bind(ConnectionInjectResolver.class)
			.to(new TypeLiteral<InjectionResolver<ConnectionInject>>(){})
			.in(Singleton.class);
	}

}
