package au.com.origma.jersey.connection.jersey;

import java.sql.Connection;
import java.util.function.Function;
import java.util.function.Supplier;

import org.glassfish.hk2.api.Factory;
import org.glassfish.jersey.server.ContainerRequest;

import au.com.origma.jersey.connection.supporting.ConnectionFactoryAndDisposer;

/**
 * Wraps a ConnectionFactory for the various ways it may be used
 * @author Ben McLean
 */
public class ConnectionInjectFactory implements Factory<Connection>, Supplier<Connection>, Function<ContainerRequest, Connection>{

	ConnectionFactoryAndDisposer cf;
	
	/**
	 * Inject an AbstractConnectionFactory and Disposer
	 * @param cf The injected param
	 */
	public ConnectionInjectFactory(ConnectionFactoryAndDisposer cf) {
		super();
		this.cf = cf;
	}

	@Override
	public Connection provide() {
		return cf.getConnectionFactory().get();
	}
	
	@Override
	public void dispose(Connection conn){
		cf.getDisposer().dispose(conn);
	}

	@Override
	public Connection get() {
		return provide();
	}

	@Override
	public Connection apply(ContainerRequest t) {
		return provide();
	}

}
