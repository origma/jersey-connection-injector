package au.com.origma.jersey.connection.factories;

import java.sql.Connection;
import java.util.function.Supplier;

/**
 * Wraps some source of connections, such as a connection pool or SQL connector
 * 
 * @author Ben McLean
 */
public abstract class AbstractConnectionFactory implements Supplier<Connection>{
	
	/**
	 * What value will be inserted into ConnectionInject to get a connection from this factory
	 * @return The connection key
	 */
	public abstract String getConnectionKey();

	/**
	 * Gets a connection
	 * @return A connection
	 */
	@Override
	public abstract Connection get();

}
