package au.com.origma.jersey.connection.factories;

import java.sql.Connection;
import java.util.HashMap;

import au.com.origma.jersey.connection.supporting.ConnectionFactoryAndDisposer;
import au.com.origma.jersey.connection.supporting.DefaultConnectionDisposer;
import au.com.origma.jersey.connection.supporting.Disposer;

/**
 * Registers and manages AbstractConnectionFactories for use with ConnectionInject
 * @author Ben McLean
 */
public class ConnectionFactoryFactory{
	
	private static ConnectionFactoryFactory instance;
	
	public static ConnectionFactoryFactory getInstance(){
		if(instance == null){
			instance = new ConnectionFactoryFactory();
		}
		return instance;
	}
	
	private HashMap<String, AbstractConnectionFactory> factories;
	private static final Disposer<Connection> defaultDisposer = new DefaultConnectionDisposer();
	
	private ConnectionFactoryFactory(){
		factories = new HashMap<>();
	}
	
	/**
	 * Register a new AbstractConnectionFactory
	 * @param key The key used to identify the connection factory
	 * @param cf The AbstractConnectionFactory
	 */
	public void register(String key, AbstractConnectionFactory cf){
		factories.put(key, cf);
	}
	
	/**
	 * Deregister an AbstractConnectionFactory
	 * @param key The key of the AbstractConnectionFactory
	 */
	public void deregister(String key){
		factories.remove(key);
	}
	
	/**
	 * Returns the AbstractConnectionFactory corresponding to the key, and a corresponding disposal object
	 * @param key
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ConnectionFactoryAndDisposer get(String key){
		AbstractConnectionFactory cf = factories.get(key);
		
		if(cf == null){
			return null;
		}
		
		if(cf instanceof Disposer<?>){
			try{
				return new ConnectionFactoryAndDisposer(cf, (Disposer<Connection>) cf);
			}catch(ClassCastException e){
				return new ConnectionFactoryAndDisposer(cf, defaultDisposer);
			}
		}else{
			return new ConnectionFactoryAndDisposer(cf, defaultDisposer);
		}
	}

}
