package au.com.origma.jersey.connection.supporting;

/**
 * Defines a way that an object can be disposed once it has been used
 * 
 * @author Ben McLean
 *
 * @param <T> The object type that this disposer covers
 */
public interface Disposer<T> {

	/**
	 * Dispose of the object
	 * @param o The object to dispose
	 */
	public void dispose(T o);
	
}
