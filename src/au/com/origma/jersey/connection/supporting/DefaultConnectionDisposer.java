package au.com.origma.jersey.connection.supporting;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * A fallback connection disposer if one is not explicitly defined
 * @author Ben McLean
 */
public class DefaultConnectionDisposer implements Disposer<Connection>{

	@Override
	public void dispose(Connection o) {
		if(o != null){
			try {
				o.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
