package au.com.origma.jersey.connection.supporting;

import java.sql.Connection;

import au.com.origma.jersey.connection.factories.AbstractConnectionFactory;

public class ConnectionFactoryAndDisposer {
	
	AbstractConnectionFactory cf;
	Disposer<Connection> disposer;
	
	public ConnectionFactoryAndDisposer(AbstractConnectionFactory cf, Disposer<Connection> disposer) {
		super();
		this.cf = cf;
		this.disposer = disposer;
	}

	public AbstractConnectionFactory getConnectionFactory() {
		return cf;
	}

	public void setConnectionFactory(AbstractConnectionFactory cf) {
		this.cf = cf;
	}

	public Disposer<Connection> getDisposer() {
		return disposer;
	}

	public void setDisposer(Disposer<Connection> disposer) {
		this.disposer = disposer;
	}
	
	

}
